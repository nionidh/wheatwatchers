FROM docker.io/lukemathwalker/cargo-chef:latest-rust-1.64-alpine3.15 as chef
RUN apk add --no-cache sqlite libc-dev pkgconfig openssl-dev
WORKDIR /app

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef as builder
COPY --from=planner /app/recipe.json recipe.json
ENV RUSTFLAGS=-Ctarget-feature=-crt-static
ENV DATABASE_URL=sqlite:build.db
RUN cargo chef cook --release --recipe-path recipe.json && \
  cargo install sqlx-cli && \
  sqlx database create
COPY . .
RUN sqlx migrate run
RUN  cargo build --release

# FROM docker.io/rust:1.64.0-alpine3.15 as builder
# WORKDIR /usr/src/wheatwatchers
# COPY . .
# RUN DATABASE_URL=sqlite:bread.db cargo install --path .

FROM docker.io/alpine:3.15
RUN apk add --no-cache libgcc
RUN mkdir -p /wheatwatchers/data
WORKDIR /wheatwatchers
COPY --from=builder /app/target/release/wheatwatchers /usr/local/bin/wheatwatchers
CMD ["/usr/local/bin/wheatwatchers"]
