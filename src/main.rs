use std::sync::Arc;

use tokio::sync::Mutex;

use crate::breadabase::SqliteDatabase;

mod breadabase;
mod server;
mod tgbot;

pub type WheatContext = Arc<Mutex<SqliteDatabase>>;

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();
    env_logger::init();
    log::info!("Starting throw dice bot...");
    let db = SqliteDatabase::new(
        &std::env::var("DATABASE_URL").expect("Please specify path to the sqlite database"),
    )
    .await;
    let db_mtx = Arc::new(Mutex::new(db));

    let (_, r) = tokio::join!(tgbot::run(db_mtx.clone()), server::run(db_mtx.clone()));
    r.unwrap();
}
