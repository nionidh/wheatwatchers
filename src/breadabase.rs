use async_trait::async_trait;
use serde::Serialize;
use sqlx::{migrate::MigrateDatabase, sqlite::SqlitePoolOptions, Pool, Sqlite};
use tokio::io;

#[derive(Debug, Clone, Serialize)]
pub struct Breadjoke {
    pub message: String,
}

#[async_trait]
pub trait Breadabase {
    async fn enter(&mut self, msg: Breadjoke) -> tokio::io::Result<()>;
    async fn list(&mut self) -> Vec<Breadjoke>;
}

pub struct SqliteDatabase {
    pool: Pool<Sqlite>,
}

impl SqliteDatabase {
    pub async fn new(path: &str) -> Self {
        Sqlite::create_database(path).await.unwrap();

        let pool = SqlitePoolOptions::new()
            .max_connections(1)
            .connect(path)
            .await
            .unwrap();

        sqlx::migrate!().run(&pool).await.unwrap();

        sqlx::query("CREATE TABLE IF NOT EXISTS bread (message TEXT NOT NULL)")
            .execute(&pool)
            .await
            .unwrap();

        SqliteDatabase { pool }
    }
}

#[async_trait]
impl Breadabase for SqliteDatabase {
    async fn enter(&mut self, msg: Breadjoke) -> io::Result<()> {
        sqlx::query!("INSERT INTO bread(message) VALUES (?1)", msg.message)
            .execute(&self.pool)
            .await
            .unwrap();
        log::info!("Recorded new breadjoke: {:?}", msg);
        Ok(())
    }

    async fn list(&mut self) -> Vec<Breadjoke> {
        let recs = sqlx::query!("SELECT message FROM bread")
            .fetch_all(&self.pool)
            .await
            .unwrap();

        recs.into_iter()
            .map(|record| Breadjoke {
                message: record.message,
            })
            .collect::<Vec<_>>()
    }
}
