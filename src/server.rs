use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use tokio::io;

use crate::{breadabase::Breadabase, WheatContext};

pub async fn run(db: WheatContext) -> Result<(), io::Error> {
    let server = HttpServer::new(move || {
        App::new()
            .service(hello)
            .service(bread_list)
            .app_data(web::Data::new(db.clone()))
    })
    .bind(("0.0.0.0", 8080))
    .unwrap();

    server.run().await
}

#[get("/hello")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[get("/bread")]
async fn bread_list(data: web::Data<WheatContext>) -> impl Responder {
    let mut db = data.lock().await;

    web::Json(db.list().await)
}
