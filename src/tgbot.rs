use std::{convert::TryFrom, error::Error, sync::Arc};

use log::warn;
use teloxide::{dispatching::UpdateFilterExt, prelude::*, utils::command::BotCommands};
use tokio::{io, sync::Mutex};

impl TryFrom<&Message> for Breadjoke {
    type Error = String;

    fn try_from(value: &Message) -> Result<Self, Self::Error> {
        if let Some(text) = value.text() {
            Ok(Breadjoke {
                message: text.to_string(),
            })
        } else {
            Err(format!(
                "Only text bread jokes are supported as of now, if u want more please annoy Mona"
            ))
        }
    }
}

use crate::{
    breadabase::{Breadabase, Breadjoke, SqliteDatabase},
    WheatContext,
};

#[derive(BotCommands, Clone, Debug)]
#[command(rename = "lowercase", description = "These commands are supported:")]
enum Command {
    #[command(description = "mark a message as a breadjoke")]
    Bj,
}

async fn answer<D: Breadabase>(
    bot: AutoSend<Bot>,
    message: Message,
    db: Arc<Mutex<D>>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    if let Some(text) = message.text() {
        if let Ok(command) = Command::parse(text, "wheatwatchers_bot") {
            match command {
                Command::Bj => {
                    if let Some(joke_message) = message.reply_to_message() {
                        match Breadjoke::try_from(joke_message) {
                            Ok(breadjoke) => {
                                let mut db = db.lock().await;
                                match db.enter(breadjoke).await {
                                    Ok(_) => {
                                        bot.send_message(message.chat.id, format!("Yup…")).await?;
                                    }
                                    Err(error) => {
                                        warn!(
                                            "Error ocurred while recording bread joke {:?}",
                                            error
                                        );
                                        bot.send_message(
                                            message.chat.id,
                                            format!("Error occurred while recording bread joke."),
                                        )
                                        .await?;
                                    }
                                }
                            }
                            Err(err) => {
                                bot.send_message(message.chat.id, err).await?;
                            }
                        }
                    } else {
                        bot.send_message(
                            message.chat.id,
                            format!("Please use /bj as a reply to the breadjoke."),
                        )
                        .await?;
                    }
                }
            }
        }
    }
    Ok(())
}

pub async fn run(db: WheatContext) -> Result<(), io::Error> {
    let bot = Bot::from_env().auto_send();

    let handler =
        dptree::entry().branch(Update::filter_message().endpoint(answer::<SqliteDatabase>));

    let mut dispatcher = Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![db])
        .enable_ctrlc_handler()
        .build();

    dispatcher.dispatch().await;

    Ok(())
}
